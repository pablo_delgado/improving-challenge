package com.improving.improvingchallenge.domain

import com.improving.improvingchallenge.data.MovieRepository
import com.improving.improvingchallenge.data.movies.model.MoviesData

class GetPopularMoviesUseCase {

    private val repository = MovieRepository()

    suspend operator fun invoke(): List<MoviesData>? = repository.getPopularMovies()

}