package com.improving.improvingchallenge.data.api

import com.improving.improvingchallenge.data.movies.model.TmdbData
import retrofit2.Response
import retrofit2.http.GET

interface ApiService {
    @GET("movie/popular?api_key=066f6c2daea7ca91f8844619dbc7f8b4")
    suspend fun getPopularMovies():Response<TmdbData>

    @GET("movie/top_rated?api_key=066f6c2daea7ca91f8844619dbc7f8b4")
    suspend fun getTopRatedMovies():Response<TmdbData>
}