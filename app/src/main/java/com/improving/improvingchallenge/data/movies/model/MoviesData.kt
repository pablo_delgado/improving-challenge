package com.improving.improvingchallenge.data.movies.model

import com.google.gson.annotations.SerializedName

data class MoviesData (
    var id: Int,
    @SerializedName("original_title") var title: String,
    var overview: String,
    @SerializedName("vote_average") var voteAverage: Double,
    @SerializedName("poster_path") var poster: String,
    @SerializedName("release_date") var releaseDate: String
    )