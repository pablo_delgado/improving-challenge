package com.improving.improvingchallenge.data.movies.model

data class TmdbData (
    var results: List<MoviesData>
    )