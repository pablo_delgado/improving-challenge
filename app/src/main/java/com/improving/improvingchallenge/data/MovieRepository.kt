package com.improving.improvingchallenge.data

import com.improving.improvingchallenge.data.api.MovieService
import com.improving.improvingchallenge.data.movies.model.MovieProvider
import com.improving.improvingchallenge.data.movies.model.MoviesData
import com.improving.improvingchallenge.utils.Const

class MovieRepository {

    private val api = MovieService()

    suspend fun getPopularMovies(): List<MoviesData> {
        val response = api.getMovies(Const.MOVIE_TYPE_POPULAR)
        MovieProvider.movies = response.results
        return response.results
    }

    suspend fun getTopRatedMovies(): List<MoviesData> {
        val response = api.getMovies(Const.MOVIE_TYPE_TOP_RATED)
        MovieProvider.movies = response.results
        return response.results
    }

}