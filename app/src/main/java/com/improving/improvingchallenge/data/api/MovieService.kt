package com.improving.improvingchallenge.data.api

import com.improving.improvingchallenge.core.RetrofitHelper
import com.improving.improvingchallenge.data.movies.model.TmdbData
import com.improving.improvingchallenge.utils.Const
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response

class MovieService {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getMovies(type: String): TmdbData {
        return withContext(Dispatchers.IO) {
            var call: Response<TmdbData>? = null
            when(type) {
                Const.MOVIE_TYPE_POPULAR -> call = retrofit.create(ApiService::class.java).getPopularMovies()
                Const.MOVIE_TYPE_TOP_RATED -> call = retrofit.create(ApiService::class.java).getTopRatedMovies()
            }
            call?.body() ?: TmdbData(emptyList())
        }
    }

}