package com.improving.improvingchallenge.ui.movies.container

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.improving.improvingchallenge.R
import com.improving.improvingchallenge.data.movies.model.MoviesData

class MovieAdapter (var onItemClick: ((MoviesData) -> Unit), private val movies: List<MoviesData>): RecyclerView.Adapter<MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MovieViewHolder(layoutInflater.inflate(R.layout.movie_item, parent, false))
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val item = movies[position]
        holder.bind(onItemClick, item.poster, movies[position])
    }

    override fun getItemCount(): Int {
        return movies.size
    }


}