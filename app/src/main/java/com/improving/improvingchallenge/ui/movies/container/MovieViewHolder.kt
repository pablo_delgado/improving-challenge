package com.improving.improvingchallenge.ui.movies.container

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.improving.improvingchallenge.data.movies.model.MoviesData
import com.improving.improvingchallenge.databinding.MovieItemBinding
import com.squareup.picasso.Picasso

class MovieViewHolder(view: View): RecyclerView.ViewHolder(view) {

    private val binding = MovieItemBinding.bind(view)

    fun bind(onItemClick: ((MoviesData) -> Unit), image: String, movieData: MoviesData) {
        Picasso.get().load("https://image.tmdb.org/t/p/w500${image}").into(binding.ivMovie)
        itemView.setOnClickListener {
            onItemClick?.invoke(movieData)
        }
    }

}