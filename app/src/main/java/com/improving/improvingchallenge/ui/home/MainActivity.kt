package com.improving.improvingchallenge.ui.home

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.improving.improvingchallenge.R
import com.improving.improvingchallenge.databinding.ActivityMainBinding
import com.improving.improvingchallenge.ui.movies.MoviesFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private val model: MainViewModel by viewModels()
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setSupportActionBar(binding.toolbar)

        val view = binding.root
        setContentView(view)

        setupNavigation()
        setupToolbar()
        model.loadPopularMovies()
        model.moviesList.observe(this, Observer { movieList ->
            binding.pbMain.isVisible = false
            binding.errorView.isVisible = false
            if (movieList.isEmpty()) {
                binding.errorView.isVisible = true
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        binding.pbMain.isVisible = true
        when (item.itemId) {
            R.id.menu_popular -> {
                model.loadPopularMovies()
            }
            R.id.menu_top_rated -> {
                model.loadTopRatedMovies()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupToolbar() {
        supportActionBar?.title = getString(R.string.movies_title)
        val appBarConfiguration = AppBarConfiguration(navController.graph)
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
        binding.toolbar.setTitleTextColor(Color.WHITE)

        navController.addOnDestinationChangedListener {_, destination, args ->
            when (destination.id) {
                R.id.moviesFragment -> {
                    binding.toolbar.title = getString(R.string.movies_title)
                    binding.toolbar.menu.findItem(R.id.menu_popular).isVisible = true
                    binding.toolbar.menu.findItem(R.id.menu_top_rated).isVisible = true
                }
                R.id.detailsFragment -> {
                    binding.toolbar.title = getString(R.string.details_title)
                    binding.toolbar.menu.findItem(R.id.menu_popular).isVisible = false
                    binding.toolbar.menu.findItem(R.id.menu_top_rated).isVisible = false
                }
                else -> ""
            }
        }
    }

    private fun setupNavigation() {
        navController = (supportFragmentManager.findFragmentById(binding.navHostFragment.id) as NavHostFragment).navController
    }
}