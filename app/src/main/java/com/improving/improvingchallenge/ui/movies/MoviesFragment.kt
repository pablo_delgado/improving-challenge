package com.improving.improvingchallenge.ui.movies

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.improving.improvingchallenge.data.movies.model.MoviesData
import com.improving.improvingchallenge.databinding.FragmentMoviesBinding
import com.improving.improvingchallenge.ui.home.MainViewModel
import com.improving.improvingchallenge.ui.movies.container.MovieAdapter

class MoviesFragment : Fragment() {

    private var _binding: FragmentMoviesBinding? = null
    private val binding get() = _binding!!
    private val directions = MoviesFragmentDirections
    private val model: MainViewModel by activityViewModels()

    private lateinit var adapter: MovieAdapter
    private val movieList = mutableListOf<MoviesData>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMoviesBinding.inflate(inflater, container, false)

        model.moviesList.observe(viewLifecycleOwner, Observer { moviesList ->
            movieList.clear()
            movieList.addAll(moviesList)
            adapter.notifyDataSetChanged()
        })

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        val itemClickListener: (MoviesData) -> Unit = { moviesData: MoviesData ->
            model.selectMovie(moviesData)
            findNavController().navigate(
                directions.actionMoviesFragmentToDetailsFragment()
            )
        }
        adapter = MovieAdapter(itemClickListener, movieList)
        binding.rvMovies.apply {
            layoutManager = GridLayoutManager(activity, 2)
        }
        binding.rvMovies.adapter = adapter
    }
}