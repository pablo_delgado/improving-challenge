package com.improving.improvingchallenge.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.improving.improvingchallenge.data.movies.model.MoviesData
import com.improving.improvingchallenge.domain.GetPopularMoviesUseCase
import com.improving.improvingchallenge.domain.GetTopRatedMoviesUseCase
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    var getPopularMoviesUserCase = GetPopularMoviesUseCase()
    var getTopRatedMoviesUseCase = GetTopRatedMoviesUseCase()

    val moviesList = MutableLiveData<List<MoviesData>>()
    val movieSelected = MutableLiveData<MoviesData>()

    fun loadPopularMovies() {
        viewModelScope.launch {
            val result = getPopularMoviesUserCase()

            if(!result.isNullOrEmpty()) {
                moviesList.postValue(result)
            } else {
                moviesList.postValue(emptyList())
            }
        }
    }

    fun loadTopRatedMovies() {
        viewModelScope.launch {
            val result = getTopRatedMoviesUseCase()

            if(!result.isNullOrEmpty()) {
                moviesList.postValue(result)
            }
        }
    }

    fun selectMovie(movie: MoviesData) {
        movieSelected.value = movie
    }

}