package com.improving.improvingchallenge.ui.details

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.improving.improvingchallenge.R
import com.improving.improvingchallenge.data.movies.model.MoviesData
import com.improving.improvingchallenge.databinding.FragmentDetailsBinding
import com.improving.improvingchallenge.ui.home.MainViewModel
import com.squareup.picasso.Picasso
import kotlin.math.roundToInt
import kotlin.math.roundToLong

class DetailsFragment : Fragment() {

    private var _binding: FragmentDetailsBinding? = null
    private val binding get() = _binding!!
    private val model: MainViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailsBinding.inflate(inflater, container, false)

        model.movieSelected.observe(viewLifecycleOwner, Observer { movie ->
            uploadUI(movie)
        })

        return binding.root
    }

    private fun uploadUI(movie: MoviesData) {
        Picasso.get().load("https://image.tmdb.org/t/p/w500${movie.poster}").into(binding.detailsImage)
        binding.detailsTitle.text = movie.title
        binding.tvOverviewDetails.text = movie.overview
        binding.rbDetails.rating = (movie.voteAverage / 2).roundToLong().toFloat()
        binding.tvDateDetails.text = movie.releaseDate
    }
}