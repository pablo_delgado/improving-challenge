package com.improving.improvingchallenge.utils

class Const {
    companion object {
        const val MOVIE_TYPE_POPULAR = "POPULAR"
        const val MOVIE_TYPE_TOP_RATED = "TOP RATED"
    }
}