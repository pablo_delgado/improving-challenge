# Improving Challenge Code #

## Overview ##
This is my code challenge project requested by Raul from Improving. You can find the goals and instructions in his [repo](https://gitlab.com/rdelgado-improving/itexico-android-native-code-challenge/-/tree/master/)

## Description ###
* This App was developed with Kotlin and MVVM structure
* Uses movies data from TMDB
* Supports English and Spanish languages
* It was tested on Google Pixel 2 - API 21 emulator, Google Pixel 3 - API 29 emulator and Samsung A71 - API 30 device
* Uses Retrofit and Picasso as external libraries
* If there is an error getting the movies data, it will show an error view
* It shows loading widgets when it's requesting external data

## Gallery ##
![Home](screenshot1.png)
![Details](screenshot2.png)
![Error](screenshot3.png)